----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:34:41 11/13/2015 
-- Design Name: 
-- Module Name:    uart_tx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity uart_tx is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           send : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR (7 downto 0);
			  tx : out  STD_LOGIC;
           busy : out  STD_LOGIC);
end uart_tx;

architecture Behavioral of uart_tx is

-- entity
COMPONENT counter
GENERIC(
	M : INTEGER
	);
PORT(
	clk : IN std_logic;
	rst : IN std_logic;          
	tc : OUT std_logic;
	half_tc : OUT std_logic
	);
END COMPONENT;

-- signals
type TSTATE is (IDLE, S_B_START, S_B_0, S_B_1, S_B_2, S_B_3, S_B_4, S_B_5, S_B_6, S_B_7, S_B_STOP);
signal state_a : TSTATE := IDLE;
signal state_f : TSTATE := IDLE;
signal regdata_a : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
signal regdata_f : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');

signal tick : STD_LOGIC := '0';
signal start : STD_LOGIC := '0';

-- constants
constant B_IDLE : STD_LOGIC := '1';
constant B_START : STD_LOGIC := '0';
constant B_STOP : STD_LOGIC := '1';

begin

-- instantiation
Inst_counter: counter 
GENERIC MAP(
	M => 5208
)
PORT MAP(
	clk => clk,
	rst => start,
	tc => tick,
	half_tc => open 
);

-- memory
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			state_a <= IDLE;
		else
			state_a <= state_f;
			regdata_a <= regdata_f;
		end if;
	end if;
end process;		

-- next state logic
process(state_a, send, tick, data, regdata_a)
begin
	regdata_f <= regdata_a;
	case state_a is
		when IDLE =>
			if send = '1' then
				regdata_f <= data;
				state_f <= S_B_START;
			else
				state_f <= IDLE;
			end if;
		when S_B_START =>
			if tick = '1' then
				state_f <= S_B_0;
			else
				state_f <= S_B_START;
			end if;
		when S_B_0 =>
			if tick = '1' then
				state_f <= S_B_1;
			else
				state_f <= S_B_0;
			end if;
		when S_B_1 =>
			if tick = '1' then
				state_f <= S_B_2;
			else
				state_f <= S_B_1;
			end if;
		when S_B_2 =>
			if tick = '1' then
				state_f <= S_B_3;
			else
				state_f <= S_B_2;
			end if;
		when S_B_3 =>
			if tick = '1' then
				state_f <= S_B_4;
			else
				state_f <= S_B_3;
			end if;
		when S_B_4 =>
			if tick = '1' then
				state_f <= S_B_5;
			else
				state_f <= S_B_4;
			end if;
		when S_B_5 =>
			if tick = '1' then
				state_f <= S_B_6;
			else
				state_f <= S_B_5;
			end if;
		when S_B_6 =>
			if tick = '1' then
				state_f <= S_B_7;
			else
				state_f <= S_B_6;
			end if;
		when S_B_7 =>
			if tick = '1' then
				state_f <= S_B_STOP;
			else
				state_f <= S_B_7;
			end if;
		when S_B_STOP =>
			if tick = '1' then
				state_f <= IDLE;
			else
				state_f <= S_B_STOP;
			end if;
	end case;
end process;

-- output logic
process(state_a, send, tick, regdata_a)
begin
	start <= '0';
	busy <= '1';
	case state_a is
		when IDLE =>
			if send = '1' then
				start <= '1';
				tx <= B_START;
			else
				busy <= '0';
				tx <= '1';
			end if;
		when S_B_START =>
			if tick = '1' then
				tx <= regdata_a(0);
			else
				tx <= B_START;
			end if;
		when S_B_0 =>
			if tick = '1' then
				tx <= regdata_a(1);
			else
				tx <= regdata_a(0);
			end if;
		when S_B_1 =>
			if tick = '1' then
				tx <= regdata_a(2);
			else
				tx <= regdata_a(1);
			end if;
		when S_B_2 =>
			if tick = '1' then
				tx <= regdata_a(3);
			else
				tx <= regdata_a(2);
			end if;
		when S_B_3 =>
			if tick = '1' then
				tx <= regdata_a(4);
			else
				tx <= regdata_a(3);
			end if;
		when S_B_4 =>
			if tick = '1' then
				tx <= regdata_a(5);
			else
				tx <= regdata_a(4);
			end if;
		when S_B_5 =>
			if tick = '1' then
				tx <= regdata_a(6);
			else
				tx <= regdata_a(5);
			end if;
		when S_B_6 =>
			if tick = '1' then
				tx <= regdata_a(7);
			else
				tx <= regdata_a(6);
			end if;
		when S_B_7 =>
			if tick = '1' then
				tx <= B_STOP;
			else
				tx <= regdata_a(7);
			end if;
		when S_B_STOP =>
			if tick = '1' then
				tx <= B_IDLE;
				busy <= '0';
			else
				tx <= B_STOP;
			end if;
	end case;
end process;

end Behavioral;

