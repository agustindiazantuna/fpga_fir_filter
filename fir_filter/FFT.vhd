----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:42:47 11/30/2015 
-- Design Name: 
-- Module Name:    FFT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FFT is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           t : in  STD_LOGIC;
           q : out  STD_LOGIC);
end FFT;

architecture Behavioral of FFT is

-- signals
signal aux : std_logic := '0';

begin

process (clk)

	begin
		if rising_edge(clk) then
			if (rst = '1') then
				aux <= '0';
			elsif (t = '1') then
				aux <= not aux;
			else
				aux <= aux;
			end if;
		end if;
	end process;

	q <= aux;

end Behavioral;

