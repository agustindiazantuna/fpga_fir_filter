
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use STD.TEXTIO.all;

package myPackage_IO is

	PROCEDURE ReadFromLine_SL(l: INOUT LINE; valor: OUT STD_LOGIC);

	PROCEDURE ReadFromLine_SLV (l: INOUT LINE; valor: OUT STD_LOGIC_VECTOR) ;

	PROCEDURE ReadFromFile_SLV (FILE fid :  text  ; valor: OUT STD_LOGIC_VECTOR);

	TYPE caracteres_std_logic IS ARRAY (CHARACTER) OF STD_LOGIC;

	CONSTANT a_stdlogic : caracteres_std_logic:=
	(
		'U'=>'U', 
		'X'=>'X',
		'0'=>'0',
		'1'=>'1',
		'Z'=>'Z',
		'W'=>'W',
		'L'=>'L',
		'H'=>'H',
		'-'=>'-',
		others=>'X'
	);

end myPackage_IO;

package body myPackage_IO is

	-- Funcion para lectura de un std_logic
	PROCEDURE ReadFromLine_SL (l: INOUT LINE; valor: OUT STD_LOGIC) IS
	
		VARIABLE caracter: CHARACTER;
		VARIABLE caracter_ok: BOOLEAN;
		
	BEGIN
	
		READ(l, caracter, caracter_ok);
		IF caracter_ok THEN 
			valor:=a_stdlogic(caracter); 
		END IF;
		
	END ReadFromLine_SL;

	-- Funcion para lectura de un std_logic_vector desde una "line"
	PROCEDURE ReadFromLine_SLV (l: INOUT LINE; valor: OUT STD_LOGIC_VECTOR) IS
		
		VARIABLE caracter    : STRING(valor'RANGE);
		VARIABLE caracter_ok : BOOLEAN;
		
	BEGIN
	
		READ(l, caracter, caracter_ok);
		
		IF caracter_ok THEN 
			FOR i IN caracter'RANGE LOOP
				valor(i):=a_stdlogic(caracter(i));
			END LOOP;
		END IF;
	
	END ReadFromLine_SLV;

	-- Funcion para lectura	de un std_logic_vector desde una "line"
	PROCEDURE ReadFromFile_SLV (FILE  fid : text  ; valor: OUT STD_LOGIC_VECTOR) IS
		
		variable vector_line : line;   		
		variable  test : std_logic_vector(3 downto 0);
		
	begin
	
			readline(fid,vector_line);    
			ReadFromLine_SLV(vector_line,test);
	
	END ReadFromFile_SLV;
	
end myPackage_IO;
