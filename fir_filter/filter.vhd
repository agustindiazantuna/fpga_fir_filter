----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:41:59 11/16/2015 
-- Design Name: 
-- Module Name:    filter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity filter is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
			  led : out STD_LOGIC;
           data_in : in  STD_LOGIC;
			  data_out : out  STD_LOGIC;
			  data_out_debug : out STD_LOGIC;
           filter_time : out  STD_LOGIC);
end filter;

architecture Behavioral of filter is

	COMPONENT databus
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		ready : IN std_logic;
		data_from_uart : IN std_logic_vector(7 downto 0);
		data_out : IN std_logic;
		busy_tx : IN std_logic;
		data_from_fir : IN std_logic_vector(15 downto 0);          
		data_in : OUT std_logic;
		data_to_fir : OUT std_logic_vector(15 downto 0);
		send : OUT std_logic;
		data_to_uart : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	COMPONENT uart_rx
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		rx : IN std_logic;          
		data : OUT std_logic_vector(7 downto 0);
		ready : OUT std_logic
		);
	END COMPONENT;

	COMPONENT uart_tx
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		send : IN std_logic;
		data : IN std_logic_vector(7 downto 0);          
		tx : OUT std_logic;
		busy : OUT std_logic
		);
	END COMPONENT;

	COMPONENT fir
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		data_in : IN std_logic;
		x : IN std_logic_vector(15 downto 0);          
		data_out : OUT std_logic;
		filter_time : OUT std_logic;
		y : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;
	
	COMPONENT FFT
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;
		t : IN std_logic;          
		q : OUT std_logic
		);
	END COMPONENT;

-- Signals
signal busy_tx : std_logic := '0';
signal ready_rx : std_logic := '0';
signal send_tx : std_logic := '0';

signal data_in_fir : std_logic := '0';
signal data_out_fir : std_logic := '0';
signal data_to_fir : std_logic_vector(15 downto 0) := (others =>'0');
signal data_to_uart : std_logic_vector(7 downto 0) := (others =>'0');
signal data_from_fir : std_logic_vector(15 downto 0) := (others =>'0');
signal data_from_uart : std_logic_vector(7 downto 0) := (others =>'0');

signal sigled : std_logic := '1';				-- fft
signal sig_data_out : std_logic := '1';

begin

	Inst_databus: databus PORT MAP(
		clk => clk,
		rst => rst,
		ready => ready_rx,
		data_in => data_in_fir,
		data_from_uart => data_from_uart,
		data_to_fir => data_to_fir,
		data_out => data_out_fir,
		busy_tx => busy_tx,
		send => send_tx,
		data_from_fir => data_from_fir,
		data_to_uart => data_to_uart
	);

	Inst_uart_rx: uart_rx PORT MAP(
		clk => clk,
		rst => rst,
		rx => data_in,
		data => data_from_uart,
		ready => ready_rx
	);

	Inst_uart_tx: uart_tx PORT MAP(
		clk => clk,
		rst => rst,
		send => send_tx,
		data => data_to_uart,
		tx => sig_data_out,
		busy => busy_tx
	);

	Inst_fir: fir PORT MAP(
		clk => clk,
		rst => rst,
		data_in => data_in_fir,
		data_out => data_out_fir,
		filter_time => filter_time,
		x => data_to_fir,
		y => data_from_fir
	);

	Inst_FFT: FFT PORT MAP(
		clk => clk,
		rst => rst,
		t => sigled,
		q => led
	);

	test: process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				sigled <= '1';
			elsif ready_rx = '1' and busy_tx = '0' then
				sigled <= '1';
			else
				sigled <= '0';
			end if;
		end if;
	end process;
	
	data_out_debug <= sig_data_out;
	data_out <= sig_data_out;

end Behavioral;

