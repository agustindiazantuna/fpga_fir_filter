--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   02:06:34 11/18/2015
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/TD1_A_2015/VHDL/fir_filter/fir_tb.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fir
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY fir_tb IS
END fir_tb;
 
ARCHITECTURE behavior OF fir_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fir
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic;
         data_out : OUT  std_logic;
         x : IN  std_logic_vector(15 downto 0);
         y : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal data_in : std_logic := '0';
   signal x : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal data_out : std_logic;
   signal y : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fir PORT MAP (
          clk => clk,
          rst => rst,
          data_in => data_in,
          data_out => data_out,
          x => x,
          y => y
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1';
		data_in <= '0';
		x <= (others => '0');
		wait for 100 ns;
		
		rst <= '0';
		data_in <= '1';
		x <= "0000000000000010";	-- 2 (0002) -> 22 (0016)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000100";	-- 4 (0004) -> 144 (0090)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000001";	-- 1 (0001) -> 441 (01B9)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111110";	-- -2 (FFFE) -> 866 (0362)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111110";	-- -2 (FFFE) -> 1193 (04A9)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000001";	-- 1 (0001) -> 1094 (0446)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000000";	-- 0 (0000) -> 376 (0178)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000001";	-- 1 (0001) -> -762 (FD06)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000011";	-- 3 (0003) -> -1612 (F9B4)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111101";	-- -3 (FFFD) -> -1567 (F9E1)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000010";	-- 2 (0002) -> -240 (FF10)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111110";	-- -2 (FFFE) -> 2052 (0804)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000001";	-- 1 (0001) -> 3351 (0D17)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111010";	-- -6 (FFFA) -> 2148 (0864)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111100";	-- -4 (FFFC) -> -1530 (FA06)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000001";	-- 1 (0001) -> -5159 (EBD9)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111111";	-- -1 (FFFF) -> -6489 (E6A7)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "1111111111111100";	-- -4 (FFFC) -> -3825 (F1DF)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000000";	-- 0 (0000) -> 2405 (0965)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		data_in <= '1';
		x <= "0000000000000010";	-- 2 (0002) -> 6766 (1A6E)
		wait for clk_period;
		data_in <= '0';
		wait for 1200 ns;
		
		assert false
		report "end"
		severity failure;
   end process;

END;
