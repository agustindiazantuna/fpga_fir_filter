--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:54:43 11/20/2015
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/TD1_A_2015/VHDL/fir_filter/filter_tb.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: filter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY filter_tb IS
END filter_tb;
 
ARCHITECTURE behavior OF filter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT filter
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic;
         data_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal data_in : std_logic := '0';

 	--Outputs
   signal data_out : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: filter PORT MAP (
          clk => clk,
          rst => rst,
          data_in => data_in,
          data_out => data_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1';
		data_in <= '1';
		wait for 104 us;
		rst <= '0';
		
		data_in <= '0';	-- bit start
		wait for 104 us;
		data_in <= '0';	-- 2
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';	-- bit stop
		wait for 2*104 us;
		data_in <= '0';	-- bit start
		wait for 104 us;
		data_in <= '0';	-- 2
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';	-- bit stop
		wait for 30*104 us;
		
		data_in <= '0';	-- bit start
		wait for 104 us;
		data_in <= '0';	-- 2
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';	-- bit stop
		wait for 2*104 us;
		data_in <= '0';	-- bit start
		wait for 104 us;
		data_in <= '0';	-- 2
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '0';
		wait for 104 us;
		data_in <= '1';	-- bit stop
		wait for 30*104 us;
		
		assert false
		report "end"
		severity failure;
   end process;

END;
