----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:28:38 11/16/2015 
-- Design Name: 
-- Module Name:    binary_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.all;
use IEEE.NUMERIC_STD.ALL;

entity binary_counter is
		generic ( M : INTEGER := 4);
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           q : out  STD_LOGIC_VECTOR (integer(ceil(log2(real (M))))-1 downto 0));
end binary_counter;

architecture Behavioral of binary_counter is

-- constants
constant N : integer := integer(ceil(log2(real (M)))); 

-- signals
signal sigcount : std_logic_vector (N-1 downto 0) := (others => '0');

begin

process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			sigcount <= (others => '0');
		elsif to_integer(unsigned(sigcount)) = M then				-- Start again
			sigcount <= (others => '0');
		else
			sigcount <= std_logic_vector(unsigned(sigcount) + 1);
		end if;
	end if;
end process;

q <= sigcount;

end Behavioral;

