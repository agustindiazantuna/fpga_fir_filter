----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:19:27 11/14/2015 
-- Design Name: 
-- Module Name:    uart_rx - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity uart_rx is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           rx : in  STD_LOGIC;
           data : out  STD_LOGIC_VECTOR (7 downto 0);
           ready : out  STD_LOGIC);
end uart_rx;

architecture Behavioral of uart_rx is

-- entity
COMPONENT counter
GENERIC(
	M : INTEGER
	);
PORT(
	clk : IN std_logic;
	rst : IN std_logic;          
	tc : OUT std_logic;
	half_tc : OUT std_logic
	);
END COMPONENT;

-- signals
type TSTATE is (IDLE, W_HALF_TICK, S_B_0, S_B_1, S_B_2, S_B_3, S_B_4, S_B_5, S_B_6, S_B_7, S_B_STOP);
signal state_a : TSTATE := IDLE;
signal state_f : TSTATE := IDLE;

signal tick : STD_LOGIC := '0';
signal half_tick : STD_LOGIC := '0';
signal start : STD_LOGIC := '0';
signal regdata_a : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
signal regdata_f : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');

-- constants
constant B_START : STD_LOGIC := '0';
constant B_STOP : STD_LOGIC := '1';

begin

-- instantiation
Inst_counter: counter 
GENERIC MAP(
	M => 5208
)
PORT MAP(
	clk => clk,
	rst => start,
	tc => tick,
	half_tc => half_tick 
);

-- memory
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			state_a <= IDLE;
			regdata_a <= (others => '0');
		else
			state_a <= state_f;
			regdata_a <= regdata_f;
		end if;
	end if;
end process;

-- next state logic
process(state_a, rx, half_tick, tick)
begin
	case state_a is
		when IDLE =>
			if rx = B_START then
				state_f <= W_HALF_TICK;
			else
				state_f <= IDLE;
			end if;
		when W_HALF_TICK =>
			if half_tick = '1' then
				state_f <= S_B_0;
			else
				state_f <= W_HALF_TICK;
			end if;
		when S_B_0 =>
			if tick = '1' then
				state_f <= S_B_1;
			else
				state_f <= S_B_0;
			end if;
		when S_B_1 =>
			if tick = '1' then
				state_f <= S_B_2;
			else
				state_f <= S_B_1;
			end if;
		when S_B_2 =>
			if tick = '1' then
				state_f <= S_B_3;
			else
				state_f <= S_B_2;
			end if;
		when S_B_3 =>
			if tick = '1' then
				state_f <= S_B_4;
			else
				state_f <= S_B_3;
			end if;
		when S_B_4 =>
			if tick = '1' then
				state_f <= S_B_5;
			else
				state_f <= S_B_4;
			end if;
		when S_B_5 =>
			if tick = '1' then
				state_f <= S_B_6;
			else
				state_f <= S_B_5;
			end if;
		when S_B_6 =>
			if tick = '1' then
				state_f <= S_B_7;
			else
				state_f <= S_B_6;
			end if;
		when S_B_7 =>
			if tick = '1' then
				state_f <= S_B_STOP;
			else
				state_f <= S_B_7;
			end if;
		when S_B_STOP =>
			if tick = '1' then
				state_f <= IDLE;
			else
				state_f <= S_B_STOP;
			end if;
	end case;
end process;

-- output logic
process(state_a, rx, half_tick, tick, regdata_a)
begin
	regdata_f <= regdata_a;
	ready <= '0';
	start <= '0';
	case state_a is
		when IDLE =>
			if rx = B_START then
				start <= '1';
			end if;
		when W_HALF_TICK =>
			if half_tick = '1' then
				start <= '1';
			end if;
		when S_B_0 =>
			if tick = '1' then
				regdata_f(0) <= rx;
			else
				regdata_f(0) <= '0';
			end if;
		when S_B_1 =>
			if tick = '1' then
				regdata_f(1) <= rx;
			else
				regdata_f(1) <= '0';
			end if;
		when S_B_2 =>
			if tick = '1' then
				regdata_f(2) <= rx;
			else
				regdata_f(2) <= '0';
			end if;
		when S_B_3 =>
			if tick = '1' then
				regdata_f(3) <= rx;
			else
				regdata_f(3) <= '0';
			end if;
		when S_B_4 =>
			if tick = '1' then
				regdata_f(4) <= rx;
			else
				regdata_f(4) <= '0';
			end if;
		when S_B_5 =>
			if tick = '1' then
				regdata_f(5) <= rx;
			else
				regdata_f(5) <= '0';
			end if;
		when S_B_6 =>
			if tick = '1' then
				regdata_f(6) <= rx;
			else
				regdata_f(6) <= '0';
			end if;
		when S_B_7 =>
			if tick = '1' then
				regdata_f(7) <= rx;
			else
				regdata_f(7) <= '0';
			end if;
		when S_B_STOP =>
			if tick = '1' then
				ready <= '1';
			end if;
	end case;
end process;

data <= regdata_a;
		
end Behavioral;

