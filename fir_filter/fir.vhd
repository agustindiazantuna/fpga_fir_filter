----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:51:41 11/15/2015 
-- Design Name: 
-- Module Name:    fir - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity fir is
	generic (W1 : INTEGER := 16;					-- Input bit width
				W2 : INTEGER := 32;					-- Multiplier bit width 2*W1
				W3 : INTEGER := 37;					-- Adder width = W2+log2(L)-1
				W4 : INTEGER := 16;					-- Output bit width
				L : INTEGER := 50);					-- Filter length (L downto 0)
    Port ( clk : in  STD_LOGIC;
			  rst : in STD_LOGIC;
			  data_in : in STD_LOGIC;
			  data_out : out STD_LOGIC;
			  filter_time : out STD_LOGIC;
           x : in  STD_LOGIC_VECTOR (W1-1 downto 0);
           y : out  STD_LOGIC_VECTOR (W4-1 downto 0));
end fir;

architecture Behavioral of fir is

	COMPONENT blk_mem_coef_fir
	PORT(
		clka : IN std_logic;
		addra : IN std_logic_vector(5 downto 0);          
		douta : OUT std_logic_vector(15 downto 0)
		);
	END COMPONENT;

	COMPONENT binary_counter
	GENERIC(
		M : INTEGER
		);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;          
		q : OUT std_logic_vector
		);
	END COMPONENT;
	
	COMPONENT counter
	GENERIC(
		M : INTEGER
		);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;          
		tc : OUT std_logic;
		half_tc : OUT std_logic
		);
	END COMPONENT;

	-- Constants
	
	CONSTANT N : INTEGER := integer(ceil(log2(real(L))));
	
	-- Types - Subtypes

	SUBTYPE N1BIT IS STD_LOGIC_VECTOR(W1-1 downto 0);
	SUBTYPE N2BIT IS STD_LOGIC_VECTOR(W2-1 downto 0);
	SUBTYPE N3BIT IS STD_LOGIC_VECTOR(W3-1 downto 0);

	TYPE ARRAY_N1BIT IS ARRAY (L+1 downto 0) OF N1BIT;				-- Un lugar extra por el ciclo de clock de retraso de la ROM
--	TYPE ARRAY_LOAD IS ARRAY (1 downto 0) OF N1BIT;				-- Un lugar extra por el ciclo de clock de retraso de la ROM

	-- Signals

--	constant coef0 : REAL := 0.00034576;	constant coef1 : REAL := 0.0015292;		constant coef2 : REAL := 0.0035175;
--	constant coef3 : REAL := 0.0057822;		constant coef4 : REAL := 0.0067893;		constant coef5 : REAL := 0.0051378;
--	constant coef6 : REAL := 0.00061702;	constant coef7 : REAL := -0.0047774;	constant coef8 : REAL := -0.0075741;
--	constant coef9 : REAL := -0.0075741;	constant coef10 : REAL := 0.0023083;	constant coef11 : REAL := 0.0098185;
--	constant coef12 : REAL := 0.011352;		constant coef13 : REAL := 0.0038427;	constant coef14 : REAL := -0.0091366;
--	constant coef15 : REAL := -0.018309;	constant coef16 : REAL := -0.014744;	constant coef17 : REAL := 0.0025268;
--	constant coef18 : REAL := 0.023214;		constant coef19 : REAL := 0.030775;		constant coef20 : REAL := 0.014069;
--	constant coef21 : REAL := -0.021732;	constant coef22 : REAL := -0.053526;	constant coef23 : REAL := -0.051437;
--	constant coef24 : REAL := 0.0025239;	constant coef25 : REAL := 0.099829;		constant coef26 : REAL := 0.20536;
--	constant coef27 : REAL := 0.2738;		constant coef28 : REAL := 0.2738;		constant coef29 : REAL := 0.20536;
--	constant coef30 : REAL := 0.099829;		constant coef31 : REAL := 0.0025239;	constant coef32 : REAL := -0.051437;
--	constant coef33 : REAL := -0.053526;	constant coef34 : REAL := -0.021732;	constant coef35 : REAL := 0.014069;
--	constant coef36 : REAL := 0.030775;		constant coef37 : REAL := 0.023214;		constant coef38 : REAL := 0.0025268;
--	constant coef39 : REAL := -0.014744;	constant coef40 : REAL := -0.018309;	constant coef41 : REAL := -0.0091366;
--	constant coef42 : REAL := 0.0038427;	constant coef43 : REAL := 0.011352;		constant coef44 : REAL := 0.0098185;
--	constant coef45 : REAL := 0.0023083;	constant coef46 : REAL := -0.0050202;	constant coef47 : REAL := -0.0075741;
--	constant coef48 : REAL := -0.0047774;	constant coef49 : REAL := 0.00061702;	constant coef50 : REAL := 0.0051378;
--	constant coef51 : REAL := 0.0067893;	constant coef52 : REAL := 0.0057822;	constant coef53 : REAL := 0.0035175;
--	constant coef54 : REAL := 0.0015292;	constant coef55 : REAL := 0.00034576;
	
--	constant escala : REAL := 32768.0;

--	SIGNAL regc : ARRAY_N1BIT := 
--	(std_logic_vector(to_signed(integer(round(coef0*escala)),W1)), std_logic_vector(to_signed(integer(round(coef1*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef2*escala)),W1)), std_logic_vector(to_signed(integer(round(coef3*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef4*escala)),W1)), std_logic_vector(to_signed(integer(round(coef5*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef6*escala)),W1)), std_logic_vector(to_signed(integer(round(coef7*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef8*escala)),W1)), std_logic_vector(to_signed(integer(round(coef9*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef10*escala)),W1)), std_logic_vector(to_signed(integer(round(coef11*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef12*escala)),W1)), std_logic_vector(to_signed(integer(round(coef13*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef14*escala)),W1)), std_logic_vector(to_signed(integer(round(coef15*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef16*escala)),W1)), std_logic_vector(to_signed(integer(round(coef17*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef18*escala)),W1)), std_logic_vector(to_signed(integer(round(coef19*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef20*escala)),W1)), std_logic_vector(to_signed(integer(round(coef21*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef22*escala)),W1)), std_logic_vector(to_signed(integer(round(coef23*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef24*escala)),W1)), std_logic_vector(to_signed(integer(round(coef25*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef26*escala)),W1)), std_logic_vector(to_signed(integer(round(coef27*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef28*escala)),W1)), std_logic_vector(to_signed(integer(round(coef29*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef30*escala)),W1)), std_logic_vector(to_signed(integer(round(coef31*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef32*escala)),W1)), std_logic_vector(to_signed(integer(round(coef33*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef34*escala)),W1)), std_logic_vector(to_signed(integer(round(coef35*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef36*escala)),W1)), std_logic_vector(to_signed(integer(round(coef37*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef38*escala)),W1)), std_logic_vector(to_signed(integer(round(coef39*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef40*escala)),W1)), std_logic_vector(to_signed(integer(round(coef41*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef42*escala)),W1)), std_logic_vector(to_signed(integer(round(coef43*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef44*escala)),W1)), std_logic_vector(to_signed(integer(round(coef45*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef46*escala)),W1)), std_logic_vector(to_signed(integer(round(coef47*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef48*escala)),W1)), std_logic_vector(to_signed(integer(round(coef49*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef50*escala)),W1)), std_logic_vector(to_signed(integer(round(coef51*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef52*escala)),W1)), std_logic_vector(to_signed(integer(round(coef53*escala)),W1)),
--	std_logic_vector(to_signed(integer(round(coef54*escala)),W1)), std_logic_vector(to_signed(integer(round(coef55*escala)),W1)));
	
-- x"A2"
-- B"0000_

	SIGNAL regx : ARRAY_N1BIT := (others => (others => '0'));				-- Data array
	SIGNAL regp : N2BIT := (others => '0');										-- Product array
	SIGNAL rega : N3BIT := (others => '0'); 										-- Adder array

	SIGNAL regc_aux : N1BIT := (others => '0');									-- Signal coef
	SIGNAL regx_aux : N1BIT := (others => '0');									-- Signal data
	SIGNAL rega_aux : N3BIT := (others => '0');									-- Signal adder
	SIGNAL reg_douta : N1BIT := (others => '0');									-- Signal douta

	SIGNAL clear : STD_LOGIC := '0';													-- Clear (p,a,y) when new data
	SIGNAL enable : STD_LOGIC := '0';												-- Filter time
	SIGNAL tick : STD_LOGIC := '0';
	SIGNAL count : STD_LOGIC_VECTOR (N-1 downto 0) := (others => '0');	-- Mux coef and data
	
	SIGNAL all_zeros : STD_LOGIC_VECTOR((W3-1)-W4 downto 0) := (others => '0');
	SIGNAL all_ones : STD_LOGIC_VECTOR((W3-1)-W4 downto 0) := (others => '1');
--	SIGNAL aux_load : ARRAY_LOAD := (others => (others => '0'));
	
begin

	-- Asignations
	clear <= data_in;																		-- Clear p,a,y when new data
	filter_time <= enable;
	
	regx_aux <= regx(to_integer(L+1-unsigned(count)));
--	regc_aux <= regc(to_integer(unsigned(count)));
	rega_aux <= regp(W2-1)&regp(W2-1)&regp(W2-1)&regp(W2-1)&regp(W2-1)&regp;
	regc_aux <= reg_douta;
--	aux_load <= 

	-- Instantiatons
	Inst_blk_mem_coef_fir: blk_mem_coef_fir
	PORT MAP(
		clka => clk,
		addra => count,
		douta => reg_douta
	);
	
	Inst_binary_counter: binary_counter				-- cuenta desde 0 hasta L+1 por el cero extra de la ROM
	GENERIC MAP(
		M => L + 1
	)
	PORT MAP(
		clk => clk,
		rst => data_in,
		q => count
	);
	
	Inst_counter: counter
	GENERIC MAP(
		M => L + 2
	)
	PORT MAP(
		clk => clk,
		rst => data_in,
		tc => tick,
		half_tc => open
	);

Load: process(clk)											-- Load data
begin
	if rising_edge(clk) then
		if rst = '1' then										-- Clean regx
			regx <= (others => (others => '0'));
		elsif data_in = '1' then							-- New data
			regx(L downto 0) <= x & regx(L downto 1);			-- Data shift one + retraso por ROM
		end if;
	end if; 
end process Load;

Multiplication: process(clk)														-- Multiplication p = x * c
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean regp
			regp <= (others => '0');
		elsif enable = '1' then
			regp <= std_logic_vector(signed(regx_aux) * signed(regc_aux));
		end if;
	end if;
end process;

SOP : process(clk)																	-- Compute sum-of-products
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean rega
			rega <= (others => '0');
		elsif enable = '1' then
			rega <= std_logic_vector(signed(rega) + signed(rega_aux));	-- filter adds
		end if;
	end if;
end process SOP;

Output : process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean y
			y <= (others => '0');
			data_out <= '0';
		elsif tick = '1' and enable = '1' then
			if unsigned(rega(W3-1 downto W4)) = unsigned(all_zeros) or unsigned(rega(W3-1 downto W4)) = unsigned(all_ones) then	-- Without overflow
				y <= rega(15 downto 0);												-- Exit
			elsif rega(W3-1) = '0' then											-- Overflow, y = 01111...
				y <= (15 => '0' , others => '1' );
			else																			-- Underflow, y = 10000...
				y <= (15 => '1' , others => '0');
			end if;
			data_out <= '1';
		else
			data_out <= '0';
		end if;
	end if;
end process;

en : process(clk)
begin
	if rising_edge(clk) then
		if data_in = '1' then
			enable <= '1';
		elsif tick = '1' then
			enable <= '0';
		end if;
	end if;
end process;

end Behavioral;

