--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:57:20 11/19/2015
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/TD1_A_2015/VHDL/fir_filter/databus_tb.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: databus
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY databus_tb IS
END databus_tb;
 
ARCHITECTURE behavior OF databus_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT databus
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         ready : IN  std_logic;
         data_in : OUT  std_logic;
         data_from_uart : IN  std_logic_vector(7 downto 0);
         data_to_fir : OUT  std_logic_vector(15 downto 0);
         data_out : IN  std_logic;
         busy_tx : IN  std_logic;
         send : OUT  std_logic;
         data_from_fir : IN  std_logic_vector(15 downto 0);
         data_to_uart : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal ready : std_logic := '0';
   signal data_from_uart : std_logic_vector(7 downto 0) := (others => '0');
   signal data_out : std_logic := '0';
   signal busy_tx : std_logic := '0';
   signal data_from_fir : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal data_in : std_logic;
   signal data_to_fir : std_logic_vector(15 downto 0);
   signal send : std_logic;
   signal data_to_uart : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: databus PORT MAP (
          clk => clk,
          rst => rst,
          ready => ready,
          data_in => data_in,
          data_from_uart => data_from_uart,
          data_to_fir => data_to_fir,
          data_out => data_out,
          busy_tx => busy_tx,
          send => send,
          data_from_fir => data_from_fir,
          data_to_uart => data_to_uart
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
 
   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1';
		ready <= '0';
		busy_tx <= '0';
		data_from_uart <= (others => '0');
		data_from_fir <= (others => '0');
		wait for 4*clk_period;
		rst <= '0';
		wait for clk_period;
		ready <= '1';
		data_from_uart <= x"AB";
		wait for clk_period;
		ready <= '0';
		wait for 4*clk_period;
		ready <= '1';
		data_from_uart <= x"CD";
		wait for clk_period;
		ready <= '0';
		wait for 4*clk_period;
		data_out <= '1';
		data_from_fir <= x"6789";
		wait for clk_period;
		data_out <= '0';
--		wait for 4*clk_period;
		busy_tx <= '1';
		wait for 4*clk_period;
		busy_tx <= '0';
		wait for 4*clk_period;
		busy_tx <= '1';
		wait for 4*clk_period;
		busy_tx <= '0';
		wait for 4*clk_period;
		assert false
		report "end"
		severity failure;
   end process;

END;
