----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:47:29 11/16/2015 
-- Design Name: 
-- Module Name:    databus - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity databus is
    Port ( clk : in STD_LOGIC;
			  rst : in STD_LOGIC;

           ready : in  STD_LOGIC;
			  data_in : out  STD_LOGIC;
           data_from_uart : in  STD_LOGIC_VECTOR (7 downto 0);
           data_to_fir : out  STD_LOGIC_VECTOR (15 downto 0);
			  
			  data_out : in  STD_LOGIC;
           busy_tx : in  STD_LOGIC;
           send : out  STD_LOGIC;
           data_from_fir : in  STD_LOGIC_VECTOR (15 downto 0);
           data_to_uart : out  STD_LOGIC_VECTOR (7 downto 0));
end databus;

architecture Behavioral of databus is

-- Constants
constant BIT_START : std_logic := '0';
constant BIT_STOP : std_logic := '1';

-- Signals
signal switch1 : std_logic := '0';
signal switch2 : std_logic_vector(1 downto 0) := (others => '0');
signal reg_fir2uart : std_logic_vector(15 downto 0) := (others => '0');

begin

uart2fir: process(clk)
begin
	if rising_edge(clk) then
		data_in <= '0';
		if rst = '1' then
			switch1 <= '0';
			data_to_fir <= (others => '0');
		elsif ready = '1' then
			if switch1 <= '0' then
				data_to_fir(15 downto 8) <= data_from_uart;
				switch1 <= '1';
			elsif switch1 <= '1' then
				data_to_fir(7 downto 0) <= data_from_uart;
				switch1 <= '0';
				data_in <= '1';
			end if;
		end if;
	end if;
end process;

savedata : process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			reg_fir2uart <= (others => '0');
		elsif data_out = '1' then
			reg_fir2uart <= data_from_fir;
		end if;
	end if;
end process;

fir2uart: process(clk)
begin
	if rising_edge(clk) then
		send <= '0';
		if rst = '1' then
			switch2 <= "00";
			data_to_uart <= (others => '0');
		else
			case switch2 is
				when "00" =>
					if data_out = '1' then
						switch2 <= "01";
					else
						switch2 <= "00";
					end if;
				when "01" =>
					data_to_uart <= reg_fir2uart(15 downto 8);
					switch2 <= "10";
					send <= '1';
				when "10" =>
					if busy_tx = '0' then
						data_to_uart <= reg_fir2uart(7 downto 0);
						switch2 <= "00";
						send <= '1';
					else
						switch2 <= "10";
					end if;
				when others =>
					switch2 <= "00";
				end case;
		end if;
	end if;
end process;

end Behavioral;

