----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:39:17 11/14/2015 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity counter is
		generic ( M : INTEGER := 4 );
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           tc : out  STD_LOGIC;
           half_tc : out  STD_LOGIC);
end counter;

architecture Behavioral of counter is

-- constants
constant N : INTEGER := INTEGER(ceil(log2(real (M))));

-- signals
signal count_a : STD_LOGIC_VECTOR (N-1 downto 0) := (others => '0');
signal count_f : STD_LOGIC_VECTOR (N-1 downto 0) := (others => '0');

begin

-- memory
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			count_a <= (others => '0');
		else
			count_a <= count_f;
		end if;
	end if;
end process;

-- next state logic
process(rst,count_a)
begin
	if rst = '1' or to_integer(unsigned(count_a)) = (M-1) then
		count_f <= (others => '0');
	else
		count_f <= std_logic_vector(unsigned(count_a) + 1);
	end if;
end process;

-- output logic
tc <= '1' when to_integer(unsigned(count_a)) = (M-1) else
		'0';
half_tc <= '1' when to_integer(unsigned(count_a)) = ((M-1)/2) else
				'0';

end Behavioral;
