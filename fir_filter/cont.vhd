----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:49:35 11/14/2015 
-- Design Name: 
-- Module Name:    cont - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
		generic ( M : INTEGER := 10 );
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           tc : out  STD_LOGIC;
           half_tc : out  STD_LOGIC);
end counter;

architecture Behavioral of counter is

-- constants
constant N : INTEGER := INTEGER(ceil(log2(real (M))));

-- signals
signal count_a : STD_LOGIC_VECTOR (N-1 downto 0);
signal count_f : STD_LOGIC_VECTOR (N-1 downto 0);

begin

-- memory
process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			count_a <= (others => '0');
		else
			count_a <= count_f;
		end if;
	end if;
end process;

-- next state logic
count_f <= (others => '0') when to_integer(unsigned(count_a)) = (M-1) else
				std_logic_vector(unsigned(count_a) + 1);

-- output logic
tc <= '1' when to_integer(unsigned(count_a)) = (M-1) else
		'0';
half_tc <= '1' when to_integer(unsigned(count_a)) = ((M-1)/2) else
				'0';

end Behavioral;

