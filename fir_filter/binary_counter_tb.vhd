--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:54:15 11/19/2015
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/TD1_A_2015/VHDL/fir_filter/binary_counter_tb.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: binary_counter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.math_real.all;
use IEEE.NUMERIC_STD.ALL;
 
ENTITY binary_counter_tb IS
END binary_counter_tb;
 
ARCHITECTURE behavior OF binary_counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)

-- Otra forma de declarar generic
--    COMPONENT binary_counter
--	 GENERIC(
--		M : INTEGER := 4);
--	 PORT(
--         clk : IN  std_logic;
--         rst : IN  std_logic;
--         q : OUT  std_logic_vector(integer(ceil(log2(real (M))))-1 downto 0)
--        );
--    END COMPONENT;

   constant M : INTEGER := 4; 

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal q : std_logic_vector(integer(ceil(log2(real (M))))-1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: entity work.binary_counter 
	GENERIC MAP(
			M => M
	)
	PORT MAP (
          clk => clk,
          rst => rst,
          q => q
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1';
		wait for 4*clk_period;
		rst <= '0';
		wait for 10*clk_period;
		assert false
		report "end"
		severity failure;
   end process;

END;
