--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:34:49 11/18/2015
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/TD1_A_2015/VHDL/fir_filter/uart_rx_tb.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: uart_rx
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
  
ENTITY uart_rx_tb IS
END uart_rx_tb;
 
ARCHITECTURE behavior OF uart_rx_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT uart_rx
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         rx : IN  std_logic;
         data : OUT  std_logic_vector(7 downto 0);
         ready : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal rx : std_logic := '0';

 	--Outputs
   signal data : std_logic_vector(7 downto 0);
   signal ready : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: uart_rx PORT MAP (
          clk => clk,
          rst => rst,
          rx => rx,
          data => data,
          ready => ready
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1';
		rx <= '1';
		wait for 104 us;
		rst <= '0';
		rx <= '0';
		wait for 104 us;
		rx <= '1';
		wait for 104 us;
		rx <= '0';
		wait for 104 us;
		rx <= '1';
		wait for 104 us;
		rx <= '0';
		wait for 104 us;
		rx <= '1';
		wait for 104 us;
		rx <= '0';
		wait for 104 us;
		rx <= '1';
		wait for 104 us;
		rx <= '0';
		wait for 104 us;
		rx <= '1';
		wait for 104 us;
		assert false
		report "end"
		severity failure;
   end process;

END;
